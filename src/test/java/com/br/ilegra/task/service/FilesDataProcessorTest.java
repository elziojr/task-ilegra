package com.br.ilegra.task.service;

import com.br.ilegra.task.converter.CustomerConverter;
import com.br.ilegra.task.converter.SaleConverter;
import com.br.ilegra.task.converter.SalesmanConverter;
import com.br.ilegra.task.model.fileData.FileData;
import com.br.ilegra.task.model.output.Output;
import com.br.ilegra.task.model.sales.Customer;
import com.br.ilegra.task.model.sales.Item;
import com.br.ilegra.task.model.sales.Sale;
import com.br.ilegra.task.model.sales.Salesman;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class FilesDataProcessorTest {
    private List<Sale> sales = new ArrayList<>();

    private final Long ID1 = 1L;
    private final Long ID2 = 10L;
    private final Long ID3 = 3L;
    private final String SALESMAN_NAME1 = "Roberto Carlos Patada de Canhota";
    private final String SALESMAN_NAME2 = "Antero Greco";
    private final BigDecimal PRICE_SALE_ITEM = BigDecimal.ONE;
    private final BigDecimal TOTAL_SALE1 = BigDecimal.ONE;
    private final BigDecimal TOTAL_SALE2 = BigDecimal.TEN;
    private final BigDecimal TOTAL_SALE3 = BigDecimal.valueOf(3L);
    private final String FILE_NAME = "testData";
    private final String FILE_FORMAT = ".dat";
    private final String FILE_FORMATED_NAME = FILE_NAME + FILE_FORMAT;

    @Mock
    private CustomerConverter customerConverter;

    @Mock
    private SalesmanConverter salesmanConverter;

    @Mock
    private SaleConverter saleConverter;

    @InjectMocks
    private FileDataProcessorService fileDataProcessorService;

    @Before
    public void setup() {
        this.sales.add(getSale1());
        this.sales.add(getSale2());
        this.sales.add(getSale3());
        when(this.customerConverter.convert(any())).thenReturn(getCustomers());
        when(this.salesmanConverter.convert(any())).thenReturn(getSalesmans());
        when(this.saleConverter.convert(any())).thenReturn(this.sales);
    }

    @Test
    public void shouldProcessDataAndReturnOutput() {
        FileData fileData = new FileData();
        fileData.setFileName(FILE_FORMATED_NAME);
        fileData.setData("");
        Output output = fileDataProcessorService.processData(fileData);
        assertEquals(new Integer(2), output.getAmountCustomers());
        assertEquals(new Integer(2), output.getAmountSalesmens());
        assertEquals(ID2, output.getExpensiveSale());
        assertEquals(SALESMAN_NAME1, output.getWorstSalesman());
    }

    @Test
    public void shouldGetWorstSalesman() {
        assertEquals(SALESMAN_NAME1, fileDataProcessorService.getWorstSalesman(this.sales));
    }

    @Test
    public void shouldGetTotalSalesBySalesman() {
        Map<String, BigDecimal> totalSalesBySalesman = fileDataProcessorService.getTotalSalesBySalesman(this.sales);
        assertEquals(totalSalesBySalesman.keySet().toArray()[0], SALESMAN_NAME2);
        assertEquals(totalSalesBySalesman.get(SALESMAN_NAME2), TOTAL_SALE2);
        assertEquals(totalSalesBySalesman.keySet().toArray()[1], SALESMAN_NAME1);
        assertEquals(totalSalesBySalesman.get(SALESMAN_NAME1), TOTAL_SALE1.add(TOTAL_SALE3));
    }

    @Test
    public void shouldGetExpensiveSale() {
        Long idExpensiveSale = fileDataProcessorService.getExpensiveSale(this.sales);
        assertEquals(ID2, idExpensiveSale);
    }

    @Test
    public void shouldGetOuputName() {
        String expectedName = FILE_NAME + ".done" + FILE_FORMAT;
        assertEquals(expectedName, fileDataProcessorService.getOutputDoneName(FILE_FORMATED_NAME));
    }

    private List<Customer> getCustomers() {
        List<Customer> customers = new ArrayList<>();
        Customer customer1 = new Customer();
        customer1.setBusinessArea("rural");
        customer1.setName("asd");
        customer1.setCnpj("211022861154");
        Customer customer2 = new Customer();
        customer1.setBusinessArea("administrativo");
        customer1.setName("ddd");
        customer1.setCnpj("515456645654");
        customers.add(customer1);
        customers.add(customer2);
        return customers;
    }

    private List<Salesman> getSalesmans() {
        List<Salesman> salesmans = new ArrayList<>();
        Salesman salesman1 = new Salesman();
        salesman1.setCpf("03636673009");
        salesman1.setName(SALESMAN_NAME1);
        salesman1.setSalary(new BigDecimal(5000));
        Salesman salesman2 = new Salesman();
        salesman2.setCpf("21215815645");
        salesman2.setName(SALESMAN_NAME2);
        salesman2.setSalary(new BigDecimal(6000));
        salesmans.add(salesman1);
        salesmans.add(salesman2);
        return  salesmans;
    }

    private Sale getSale1() {
        Sale sale = new Sale();
        sale.setId(ID1);
        sale.setSalesmanName(SALESMAN_NAME1);
        sale.setTotalSale(TOTAL_SALE1);
        sale.setItems(getItems1());
        return sale;
    }

    private List<Item> getItems1() {
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setId(1L);
        item.setPrice(PRICE_SALE_ITEM);
        item.setQuantity(1);
        items.add(item);
        return items;
    }

    private Sale getSale2() {
        Sale sale = new Sale();
        sale.setId(ID2);
        sale.setSalesmanName(SALESMAN_NAME2);
        sale.setTotalSale(TOTAL_SALE2);
        sale.setItems(getItems2());
        return sale;
    }

    private List<Item> getItems2() {
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setId(1L);
        item.setPrice(PRICE_SALE_ITEM);
        item.setQuantity(10);
        items.add(item);
        return items;
    }

    private Sale getSale3() {
        Sale sale = new Sale();
        sale.setId(ID3);
        sale.setSalesmanName(SALESMAN_NAME1);
        sale.setTotalSale(TOTAL_SALE3);
        sale.setItems(getItems3());
        return sale;
    }

    private List<Item> getItems3() {
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setId(1L);
        item.setPrice(PRICE_SALE_ITEM);
        item.setQuantity(3);
        items.add(item);
        return items;
    }
}
