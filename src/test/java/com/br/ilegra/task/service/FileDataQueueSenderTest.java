package com.br.ilegra.task.service;

import com.br.ilegra.task.jms.FileDataMessageSender;
import com.br.ilegra.task.model.fileData.FileData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;


@RunWith(MockitoJUnitRunner.class)
public class FileDataQueueSenderTest {
    private final String NAME = "Fausto Silva";
    private final String DATA = "001ç554564564 ";
    private FileData fileData = new FileData();

    @Mock
    private FileProcessorService fileProcessor;

    @Mock
    private FileDataMessageSender fileDataMessageSender;

    @InjectMocks
    private FileDataQueueSenderService fileDataQueueSenderService;

    @Before
    public void setup() {
        when(fileProcessor.getFilesData()).thenReturn(getFilesData());
    }

    @Test
    public void shouldSendFilesDataToQueue() {
        fileDataQueueSenderService.sendFilesDataToQueue();
        verify(fileProcessor).getFilesData();
        verify(fileDataMessageSender).send(eq(this.fileData));
    }

    private List<FileData> getFilesData() {
        List<FileData> filesData = new ArrayList<>();
        fileData.setFileName(NAME);
        fileData.setData(DATA);
        filesData.add(fileData);
        return filesData;
    }
}
