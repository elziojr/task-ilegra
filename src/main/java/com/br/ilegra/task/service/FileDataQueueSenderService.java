package com.br.ilegra.task.service;

import com.br.ilegra.task.jms.FileDataMessageSender;
import com.br.ilegra.task.model.fileData.FileData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableScheduling
public class FileDataQueueSenderService {

    private final long ONE_SECOND = 1000;

    @Autowired
    private FileProcessorService fileProcessor;

    @Autowired
    private FileDataMessageSender fileDataMessageSender;

    @Scheduled(fixedDelay = ONE_SECOND)
    public void sendFilesDataToQueue() {
        List<FileData> filesData = fileProcessor.getFilesData();
        filesData.forEach(fileData -> fileDataMessageSender.send(fileData));
    }
}
