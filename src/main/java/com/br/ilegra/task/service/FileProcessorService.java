package com.br.ilegra.task.service;

import com.br.ilegra.task.model.fileData.FileData;
import com.br.ilegra.task.model.output.Output;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.*;

@Component
public class FileProcessorService {

    private final String HOME_DIRECTORY = System.getProperty("user.home");
    private final Charset CHARSET_UTF8 = Charsets.UTF_8;
    private final String BAR = File.separator;

    public void writeOutputFileData(Output output) throws Exception {
        File outputFile = new File(output.getName());
        outputFile.getParentFile().mkdirs();
        PrintWriter printer = new PrintWriter(outputFile);
        printer.println("Amount of customers in the input file: " + output.getAmountCustomers());
        printer.println("Amount of salesman in the input file: " + output.getAmountSalesmens());
        printer.println("ID of the most expensive sale: " + output.getExpensiveSale());
        printer.println("Worst salesman ever: " + output.getWorstSalesman());
        printer.close();
    }

    public List<FileData> getFilesData() {
        List<File> files = getFiles();
        List<FileData> filesData = new ArrayList<>();
        for (File file : files) {
            try {
                FileData fData = new FileData();
                fData.setFileName(file.getAbsolutePath().replace(BAR + "in" + BAR, BAR + "out" + BAR));
                fData.setData(new String(Files.toString(file, CHARSET_UTF8)));
                filesData.add(fData);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        renameFilesToRead(files);
        return filesData;
    }

    private List<File> getFiles() {
        File homeDirectory = new File(HOME_DIRECTORY + BAR + "data" + BAR + "in");
        File[] files = homeDirectory.listFiles((dir, name) -> name.endsWith(".dat"));
        return files != null ? Arrays.asList(files) : new ArrayList<>();
    }

    private void renameFilesToRead(List<File> files) {
        files.forEach(file -> file.renameTo(new File(file.getAbsolutePath() + ".read")));
    }
}
