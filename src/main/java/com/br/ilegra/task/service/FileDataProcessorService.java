package com.br.ilegra.task.service;

import com.br.ilegra.task.converter.CustomerConverter;
import com.br.ilegra.task.converter.SaleConverter;
import com.br.ilegra.task.converter.SalesmanConverter;
import com.br.ilegra.task.model.fileData.FileData;
import com.br.ilegra.task.model.output.Output;
import com.br.ilegra.task.model.sales.Customer;
import com.br.ilegra.task.model.sales.Sale;
import com.br.ilegra.task.model.sales.Salesman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class FileDataProcessorService {

    @Autowired
    private CustomerConverter customerConverter;

    @Autowired
    private SalesmanConverter salesmanConverter;

    @Autowired
    private SaleConverter saleConverter;

    public Output processData(final FileData fileData) {
        String data = fileData.getData();
        List<Salesman> salesmans = getSalesmansFromData(data);
        List<Customer> customers = getCustomersFromData(data);
        List<Sale> sales = getSalesFromData(data);
        saleConverter.defineSalesmanForSale(sales, salesmans);

        return Output.builder()
                .name(getOutputDoneName(fileData.getFileName()))
                .amountCustomers(customers.size())
                .amountSalesmens(salesmans.size())
                .expensiveSale(getExpensiveSale(sales))
                .worstSalesman(getWorstSalesman(sales))
                .build();
    }

    public String getWorstSalesman(List<Sale> sales) {
        return getTotalSalesBySalesman(sales)
                .entrySet().stream()
                .reduce((first, second) -> {
                    Boolean firsIsSmaller = first.getValue().compareTo(second.getValue()) == -1;
                    return firsIsSmaller ? first : second;
                }).get()
                .getKey();
    }

    public Map<String, BigDecimal> getTotalSalesBySalesman(List<Sale> sales) {
        Map<String, BigDecimal> totalSalesBySalesman = new HashMap<>();
        sales.forEach(sale -> {
            if (!totalSalesBySalesman.containsKey(sale.getSalesmanName())) {
                totalSalesBySalesman.put(sale.getSalesmanName(), sale.getTotalSale());
                return;
            }

            BigDecimal sumTotal = totalSalesBySalesman.get(sale.getSalesmanName()).add(sale.getTotalSale());
            totalSalesBySalesman.put(sale.getSalesmanName(), sumTotal);
        });

        return totalSalesBySalesman;
    }

    public String getOutputDoneName(String oldName) {
        String name = oldName.substring(0, oldName.indexOf(".dat"));
        name += ".done.dat";
        return name;
    }

    public Long getExpensiveSale(List<Sale> sales) {
        Sale expensiveSale = new Sale();
        expensiveSale.setId(0L);
        expensiveSale.setTotalSale(BigDecimal.ZERO);

        return sales.stream()
                .max(Comparator.comparing(Sale::getTotalSale))
                .get()
                .getId();
    }

    private List<Sale> getSalesFromData(String data) {
        return saleConverter.convert(data);
    }

    private List<Customer> getCustomersFromData(String data) {
        return customerConverter.convert(data);
    }

    private List<Salesman> getSalesmansFromData(String data) {
        return salesmanConverter.convert(data);
    }
}
