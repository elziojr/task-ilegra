package com.br.ilegra.task.jms;

import com.br.ilegra.task.model.fileData.FileData;
import org.springframework.stereotype.Component;

import static com.br.ilegra.task.TaskApplication.jmsTemplate;

@Component
public class FileDataMessageSender {
    public void send(FileData fileData) {
        jmsTemplate.convertAndSend("filesqueue", fileData);
    }
}
