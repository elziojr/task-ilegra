package com.br.ilegra.task.jms;

import com.br.ilegra.task.model.fileData.FileData;
import com.br.ilegra.task.model.output.Output;
import com.br.ilegra.task.service.FileDataProcessorService;
import com.br.ilegra.task.service.FileProcessorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FileDataMessageReceiver {

    @Autowired
    private FileDataProcessorService fileDataProcessorService;

    @Autowired
    private FileProcessorService fileProcessor;

    @JmsListener(destination = "filesqueue", containerFactory = "messagesFactory")
    public void receiveDataMessage(FileData fileData) {
        Output outputFileData = fileDataProcessorService.processData(fileData);
        try {
            fileProcessor.writeOutputFileData(outputFileData);
            log.info("Arquivo " + outputFileData.getName() + " escrito com sucesso!");
        } catch (Exception e) {
            log.error("Erro ao escrever arquivo final", e);
        }
    }

}