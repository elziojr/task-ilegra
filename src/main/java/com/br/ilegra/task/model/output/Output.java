package com.br.ilegra.task.model.output;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Output {
    private String name;
    @Builder.Default
    private Integer amountCustomers = 0;
    @Builder.Default
    private Integer amountSalesmens = 0;
    private Long expensiveSale;
    private String worstSalesman;
}
