package com.br.ilegra.task.converter;

import com.br.ilegra.task.model.sales.Item;
import com.br.ilegra.task.model.sales.Sale;
import com.br.ilegra.task.model.sales.Salesman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SaleConverter implements ConverterInterface<String, Sale> {

    @Autowired
    private ItemConverter itemConverter;

    private final String DELIMITER = "003ç";
    private final String SPLIT_DELIMITER = "ç";

    public void defineSalesmanForSale(List<Sale> sales, List<Salesman> salesmens) {
        sales.forEach(sale -> sale.setSalesman(getSalesmanFromList(sale.getSalesmanName(), salesmens)));
    }

    @Override
    public List<Sale> convert(String data) {
        List<Sale> sale = new ArrayList<>();

        String regex = String.format("%s((?:(?!%s|\\n).)*)", DELIMITER, DELIMITER);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(data);

        while(matcher.find()) {
            sale.add(getSaleFromRow(matcher.group(1)));
        }

        return sale;
    }

    private Sale getSaleFromRow(String dataRow) {
        List<Item> saleItems = getSaleItems(dataRow);
        return Sale.builder()
                .id(Long.valueOf(dataRow.substring(0, dataRow.indexOf(SPLIT_DELIMITER))))
                .items(saleItems)
                .salesmanName(dataRow.substring(dataRow.lastIndexOf(SPLIT_DELIMITER) + 1))
                .totalSale(getTotalItemsPrice(saleItems))
                .build();
    }

    private BigDecimal getTotalItemsPrice(List<Item> items) {
        BigDecimal total = BigDecimal.ZERO;
        for (Item item : items) {
            total = total.add(item.getPrice().multiply(new BigDecimal(item.getQuantity())));
        }

        return total;
    }

    private List<Item> getSaleItems(String dataRow) {
        String itemRow = dataRow.substring(dataRow.indexOf("[") + 1, dataRow.lastIndexOf("]"));
        return itemConverter.convert(itemRow);
    }

    private Salesman getSalesmanFromList(String name, List<Salesman> salesmens) {
        Optional<Salesman> salesman = salesmens.stream()
                .filter(item -> item.getName().equals(name))
                .findFirst();

        return salesman.isPresent()
                ? salesman.get()
                : null;
    }
}
