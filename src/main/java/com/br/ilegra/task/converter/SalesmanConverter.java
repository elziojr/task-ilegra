package com.br.ilegra.task.converter;

import com.br.ilegra.task.model.sales.Salesman;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SalesmanConverter implements ConverterInterface<String, Salesman> {

    private final String DELIMITER = "001ç";
    private final String SPLIT_DELIMITER = "ç";

    @Override
    public List<Salesman> convert(String data) {
        List<Salesman> salesmens = new ArrayList<>();

        String regex = String.format("%s((?:(?!%s|\\n).)*)", DELIMITER, DELIMITER);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(data);

        while(matcher.find()) {
            salesmens.add(getSalesmanFromRow(matcher.group(1)));
        }

        return salesmens;
    }

    private Salesman getSalesmanFromRow(String dataRow) {
        return Salesman.builder()
                .cpf(dataRow.substring(0, dataRow.indexOf(SPLIT_DELIMITER)))
                .name(dataRow.substring(dataRow.indexOf(SPLIT_DELIMITER) + 1, dataRow.lastIndexOf(SPLIT_DELIMITER)))
                .salary(getSalary(dataRow))
                .build();
    }

    private BigDecimal getSalary(String dataRow) {
        return new BigDecimal(dataRow.substring(dataRow.lastIndexOf(SPLIT_DELIMITER) + 1).trim());
    }
}
