package com.br.ilegra.task.converter;

import com.br.ilegra.task.model.sales.Customer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CustomerConverter implements ConverterInterface<String, Customer> {

    private final String DELIMITER = "002ç";
    private final String SPLIT_DELIMITER = "ç";

    @Override
    public List<Customer> convert(String data) {
        List<Customer> customers = new ArrayList<>();

        String regex = String.format("%s((?:(?!%s|\\n).)*)", DELIMITER, DELIMITER);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(data);

        while(matcher.find()) {
            customers.add(getCustomerFromRow(matcher.group(1)));
        }

        return customers;
    }

    private Customer getCustomerFromRow(String dataRow) {
        return Customer.builder()
                .cnpj(dataRow.substring(0, dataRow.indexOf(SPLIT_DELIMITER)))
                .name(dataRow.substring(dataRow.indexOf(SPLIT_DELIMITER) + 1, dataRow.lastIndexOf(SPLIT_DELIMITER)))
                .businessArea(dataRow.substring(dataRow.lastIndexOf(SPLIT_DELIMITER) + 1))
                .build();
    }
}
