package com.br.ilegra.task.converter;

import java.util.List;

public interface ConverterInterface<T, E> {
    List<E> convert(T t);
}
