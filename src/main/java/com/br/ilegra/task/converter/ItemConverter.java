package com.br.ilegra.task.converter;

import com.br.ilegra.task.model.sales.Item;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ItemConverter implements ConverterInterface<String, Item> {

    private final String DELIMITER = "-";
    private final String SPLIT_DELIMITER = ",";

    @Override
    public List<Item> convert(String itemRow) {
        return Arrays.asList(itemRow.split(SPLIT_DELIMITER))
                .stream()
                .map(data -> Item.builder()
                            .id(Long.valueOf(data.substring(0, data.indexOf(DELIMITER))))
                            .quantity(Integer.valueOf(data.substring(data.indexOf(DELIMITER) + 1, data.lastIndexOf(DELIMITER))))
                            .price(new BigDecimal(data.substring(data.lastIndexOf(DELIMITER) + 1)))
                            .build())
                .collect(Collectors.toList());
    }
}
