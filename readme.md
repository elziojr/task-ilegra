# Task Ilegra
This test is written just to solve a very simple problem with system files.<br>
To run this project execute the following command in path:
<br>
`./gradlew bootRun`
<br><br>
Are been written just some tests to show how I think about this<br>

#### To see my knowledge with devOps, cloud, CI, CD, please look the following projects:
<a href="https://gitlab.com/elziojr/laa">Log Access Analytics with Infrastructure as a Code</a> <br>
<a href="https://gitlab.com/elziojr/jhipster-playground">JHipster Poc with Netflix OSS</a>